from googletrans import Translator


class TranslateClass:
    """
    Класс для перевода текста с одного языка на другой.
    """
    def __init__(self, src: str = 'ru', dest: str = 'en'):
        """
        Инициализация класса.
        :param src: Язык, с которого надо переводить (изначальный язык).
        :param dest: Язык, на который надо переводить (выходной язык).
        """
        self.src = src
        self.dest = dest

    def translate(self, text: str) -> str:
        """
        Метод, который осуществляет перевод.
        :param text: Текст на искомом языке.
        :return: Переведенный текст на искомом языке.
        """
        translator = Translator()
        translation = translator.translate(text, src=self.src, dest=self.dest)
        return translation.text

